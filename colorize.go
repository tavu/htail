package main

import "fmt"

// Colorize to make stdout more informative
type Colorize struct {
	Host string
}

func (s Colorize) Write(p []byte) (n int, err error) {
	fmt.Printf("%s: %s", s.Host, p)
	return len(p), err
}
