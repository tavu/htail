package main

import "strings"

// Host type to store target hosts
type Host []string

func (e *Host) String() string {
	return strings.Join(*e, "\n")
}

// Set appends target to Host slice
func (e *Host) Set(value string) error {
	*e = append(*e, value)
	return nil
}

// Type Returns value type
func (e *Host) Type() string {
	return "string"
}

func (e *Host) len() int {
	return len(*e)
}
