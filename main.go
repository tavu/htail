package main

import (
	"fmt"
	"os"
	"strings"
	"sync"

	"github.com/spf13/cobra"
)

var verbose bool
var target Host
var privateKeyFile string

var rootCmd = &cobra.Command{
	Use:   "htail [OPTION]... [FILE]...",
	Short: "htail allows you to output the last part of files on remote hosts",
	Long:  `htail allows you to output the last part of files on remote hosts`,
	Example: `$ htail FILE1 FILE2 ...
  $ htail -t host1 -t host2 -i id_dsa -f /var/log/mail.log
  $ htail -r psql -d dbname -c "SELECT host FROM table" -i id_dsa -f /var/log/mail.log`,
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("0. amount of targets: %d\n", len(target))
		fmt.Println(target.String())
		fmt.Println("Yo-yo. Doing something here.")
	},
}

func init() {
	usageTempl := `{{ $cmd := . }}Usage:
  {{.UseLine}}

Flags:
{{.LocalFlags.FlagUsages}}
Examples:
  {{ .Example }}
`
	rootCmd.SetUsageTemplate(usageTempl)
	rootCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "verbose output")
	rootCmd.Flags().VarP(&target, "target", "t", "target host")
	rootCmd.Flags().StringVarP(&privateKeyFile, "identity", "i", "", "identity file")
}

// Execute is the start were all begins
func main() {
	// go func() {}()
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println(strings.Repeat("-", 76))
	fmt.Printf("Amount of targets: %d\n", len(target))
	//fmt.Printf("amount of targets: %d\n", cap(target))
	for i, v := range target {
		fmt.Printf("- target %d: %s\n", i+1, v)
	}
	fmt.Printf("PrivateKeyFile: %s\n", privateKeyFile)

	var wg sync.WaitGroup
	for _, t := range target {
		fmt.Println("Host: " + t)
		wg.Add(1)
		go func(host string) {
			defer wg.Done()
			client, err := EstablishClientTo(host)
			if err != nil {
				fmt.Printf("Can't establish connection to %s: %v\n", host, err)
				return
			}
			defer func() {
				if err := client.Close(); err != nil {
					fmt.Println(err)
				}
			}()

			sess, err := client.NewSession()
			if err != nil {
				fmt.Printf("Can't create session: %s\n", err)
			}
			defer func() {
				if err := sess.Close(); err != nil {
					fmt.Println(err)
				}
			}()

			mycolorize := Colorize{
				Host: host,
			}
			sess.Stdout = mycolorize
			if err := sess.Run("/usr/bin/tail -n 1 -f /var/log/syslog"); err != nil {
				fmt.Printf("Failed to run: %s", err.Error())
			}
		}(t)
	}
	wg.Wait()

	fmt.Println("Exiting.")
}
