package main

import (
	"bufio"
	"bytes"
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/crypto/ssh"
)

// EstablishClientTo connects to remote server by method x
func EstablishClientTo(host string) (*ssh.Client, error) {
	fmt.Println("Looking host key for authentication")
	hostKey, err := getHostKey(host)
	if err != nil {
		return nil, fmt.Errorf("Can't find host key for %s: %s", host, err)
	}
	fmt.Printf("Host: %s Key: %s\n", host, hostKey)

	fmt.Println("Looking private key for authentication")
	key, err := ioutil.ReadFile(privateKeyFile)
	if err != nil {
		return nil, fmt.Errorf("Unable to read private key: %s", err)
	}
	signer, err := ssh.ParsePrivateKey(key)

	// TODO: hardcoded username currently
	config := &ssh.ClientConfig{
		User: "tojo",
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(signer),
		},
		HostKeyCallback: ssh.FixedHostKey(hostKey),
	}

	fmt.Printf("Connecting to %s\n", host)
	client, err := ssh.Dial("tcp", host+":22", config)
	if err != nil {
		return nil, fmt.Errorf("Unable to connect host %s: %s", host, err)
	}
	return client, nil
	// defer client.Close()

	// session, err := client.NewSession()
}

func getHostKey(host string) (ssh.PublicKey, error) {
	file, err := os.Open(filepath.Join(os.Getenv("HOME"), ".ssh", "known_hosts"))
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := file.Close(); err != nil {
			fmt.Printf("file close error: %s\n", err)
		}
	}()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		fields := strings.Split(scanner.Text(), " ")
		if len(fields) != 3 {
			continue
		}
		/*_, hosts, _, _, _, err := ssh.ParseKnownHosts(scanner.Bytes())
		if err != nil {
			return nil, fmt.Errorf("Can't parse known_hosts file: %s", err)
		}*/
		valid, err := isValidHash(host, scanner.Text())
		if err != nil {
			fmt.Printf("Validation check failed: %s\n", err)
		}
		if valid {
			hostKey, _, _, _, err := ssh.ParseAuthorizedKey(scanner.Bytes())
			if err != nil {
				return nil, fmt.Errorf("Error parsing key %q: %v", fields[2], err)
			}
			return hostKey, nil
		}
	}
	return nil, fmt.Errorf("No hostkey for %s", host)
}

func isValidHash(hostname string, encoded string) (bool, error) {
	if len(encoded) == 0 || encoded[0] != '|' {
		return false, fmt.Errorf("known_hosts: hashed host must start with '|'")
	}
	components := strings.Split(encoded, "|")
	if len(components) != 4 {
		return false, fmt.Errorf("known_hosts: got %d components, want 3", len(components))
	}

	salt, _ := base64.StdEncoding.DecodeString(components[2])
	hash, _ := base64.StdEncoding.DecodeString(components[3])

	mac := hmac.New(sha1.New, salt)
	_, err := mac.Write([]byte(hostname))
	if err != nil {
		return false, fmt.Errorf("hash failed: %s", err)
	}
	got := mac.Sum(nil)
	if !bytes.Equal(got, hash) {
		return false, fmt.Errorf("got hash %x want %x", got, hash)
	}
	return true, nil
}
